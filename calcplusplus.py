import sys
import calcoohija
import csv


class Calcplus(calcoohija.CalculadoraHija):
    pass


with open(sys.argv[1], newline="") as archivo:
    archivo = csv.reader(archivo)
    for num in archivo:
        "num --> numero"
        "o --> objeto"
        "al superar el limite de caracteres, debo ponerlo asi, sorry"
        contador = 1
        valor = 0
        o = Calcplus(int(num[contador]), int(num[contador + 1]))
        tamano = len(num) - 1

        while contador < tamano:
            if contador == 1:
                if num[0] == "suma":
                    valor = o.plus(int(num[contador]), int(num[contador+1]))
                elif num[0] == "resta":
                    valor = o.minus(int(num[contador]), int(num[contador+1]))
                elif num[0] == "multiplica":
                    valor = o.mult(int(num[contador]), int(num[contador+1]))
                elif num[0] == "divide":
                    valor = o.div(float(num[contador]), float(num[contador+1]))
                else:
                    sys.exit("Solo puedes +, -, x o ·/·")
                contador = contador + 1
            else:
                o.operando1 = valor
                o.operando2 = num[contador + 1]
                if num[0] == "suma":
                    valor = o.plus(int(o.operando1), int(o.operando2))
                elif num[0] == "resta":
                    valor = o.minus(int(o.operando1), int(o.operando2))
                elif num[0] == "multiplica":
                    valor = o.mult(int(o.operando1), int(o.operando2))
                elif num[0] == "divide":
                    valor = o.div(float(o.operando1), float(o.operando2))
                else:
                    sys.exit("Solo puedes +, -, x o ·/·")
                contador = contador + 1
        print(int(valor))
