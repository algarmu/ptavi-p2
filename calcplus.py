import sys
import calcoohija


class Calcplus(calcoohija.CalculadoraHija):
    pass


if __name__ == "__main__":
    fichero = open(sys.argv[1])
    elementos = fichero.readlines()
    for numero in elementos:
        line = numero.split(",")
        contador = 1
        valor = 0
        "o --> objeto"
        "al superar el limite de caracteres, debo ponerlo asi, sorry"
        o = Calcplus(int(line[contador]), int(line[contador + 1]))
        tamano = len(line) - 1

        while contador < tamano:
            if contador == 1:
                if line[0] == "suma":
                    valor = o.plus(int(line[contador]), int(line[contador+1]))
                elif line[0] == "resta":
                    valor = o.minus(int(line[contador]), int(line[contador+1]))
                elif line[0] == "multiplica":
                    valor = o.mult(int(line[contador]), int(line[contador+1]))
                elif line[0] == "divide":
                    valor = o.div(int(line[contador]), int(line[contador+1]))
                else:
                    sys.exit("Solo puedes +, -, x o ·/·")
                contador = contador + 1
            else:
                o.operando1 = valor
                o.operando2 = line[contador + 1]
                if line[0] == "suma":
                    valor = o.plus(int(o.operando1), int(o.operando2))
                elif line[0] == "resta":
                    valor = o.minus(int(o.operando1), int(o.operando2))
                elif line[0] == "multiplica":
                    valor = o.mult(int(o.operando1), int(o.operando2))
                elif line[0] == "divide":
                    valor = o.div(int(o.operando1), int(o.operando2))
                else:
                    sys.exit("Solo puedes +, -, x o ·/·")
                contador = contador + 1
        print(int(valor))
