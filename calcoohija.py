import sys


class Calculadora():
    def __init__(self, operando1, operando2):
        self.valor1 = operando1
        self.valor2 = operando2

    def plus(self, o1, o2):
        return o1 + o2

    def minus(self, o1, o2):
        return o1 - o2


class CalculadoraHija(Calculadora):
    def mult(self, o1, o2):
        return o1 * o2

    def div(self, o1, o2):
        try:
            return o1 / o2

        except ZeroDivisionError:
            sys.exit("No se puede dividir entre 0")


if __name__ == "__main__":
    try:
        o1 = int(sys.argv[1])
        o2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    objeto = CalculadoraHija(o1, o2)
    if sys.argv[2] == "mas":
        result = objeto.plus(o1, o2)
    elif sys.argv[2] == "menos":
        result = objeto.minus(o1, o2)
    elif sys.argv[2] == "por":
        result = objeto.mult(o1, o2)
    elif sys.argv[2] == "entre":
        if o2 == "0":
            sys.exit('No se puede dividir entre 0')
        else:
            result = objeto.div(o1, o2)
    else:
        sys.exit('Sólo puede ser sumar, restar, multiplicar o dividir.')
    print(result)
